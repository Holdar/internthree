//
//  DetailsUserVC.swift
//  InternThree
//
//  Created by Murtazaliev Shamil on 03/04/2019.
//  Copyright © 2019 Murtazaliev Shamil. All rights reserved.
//

import UIKit
import SDWebImage

protocol DetailsUserVCProtocol: class {
    func showLoadIndicator()
    func hiddenLoadIndicator()
    func showAlert(title: String, message: String)
    func setUrl() -> String
    func setUser(image: String, login: String, name: String, location: String, repos: String, followers: String, following: String, nameUser: String)
}

final class DetailsUserVC: UIViewController, DetailsUserVCProtocol {

    @IBOutlet weak var imageUserDetails: UIImageView!
    @IBOutlet weak var loginUserDetails: UILabel!
    @IBOutlet weak var nameUserDetails: UILabel!
    @IBOutlet weak var locationUserDetails: UILabel!
    @IBOutlet weak var reposUserDetails: UILabel!
    @IBOutlet weak var followersUserDetails: UILabel!
    @IBOutlet weak var followingUserDetails: UILabel!
    
    public var profileId: String!
    @IBOutlet weak var progressIndicator: UIActivityIndicatorView!
    @IBOutlet weak var viewIndicatorLoad: UIView!
    
    private var presenter: PresenterDetailsUserProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem?.tintColor = UIColor.black
        presenter = PresenterDetailsUser(view: self)
        presenter.getUser()
    }
    
    func setUser(image: String, login: String, name: String, location: String, repos: String, followers: String, following: String, nameUser: String) {
        imageUserDetails.sd_setImage(with: URL(string: image), completed: nil)
        loginUserDetails.text = login
        nameUserDetails.text = name
        locationUserDetails.text = location
        reposUserDetails.text = repos
        followersUserDetails.text = followers
        followingUserDetails.text = following
        navigationItem.title = nameUser
    }
    
    func setUrl() -> String {
        return profileId!
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true)
    }
    
    func showLoadIndicator() {
        viewIndicatorLoad.isHidden = false
        progressIndicator.startAnimating()
        progressIndicator.isHidden = false
    }
    
    func hiddenLoadIndicator() {
        progressIndicator.stopAnimating()
        progressIndicator.isHidden = true
        viewIndicatorLoad.isHidden = true
    }
}
