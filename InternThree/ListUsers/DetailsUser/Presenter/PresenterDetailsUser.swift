//
//  PresenterDetailsUser.swift
//  InternThree
//
//  Created by Murtazaliev Shamil on 11/04/2019.
//  Copyright © 2019 Murtazaliev Shamil. All rights reserved.
//

import Foundation

protocol PresenterDetailsUserProtocol {
    func getUser()
}

class PresenterDetailsUser: PresenterDetailsUserProtocol {
    private weak var view: DetailsUserVCProtocol!
    private var model: UserDetailsProtocol!
    
    init(view: DetailsUserVCProtocol){
        self.view = view
        model = ControllerUserDetails()
    }

    func getUserDetails(callback: @escaping(UserDetails) -> Void) {
        self.view.showLoadIndicator()
        let url = URL(string: "https://api.github.com/users/\(view.setUrl())")
        print(url!)
        model.getUserDetails(url: url!, success: { (result: UserDetails) in
            callback(result)
        }) { (failure) in
            switch failure {
            case "noResponse":
                self.view.showAlert(title: "NoResponse", message: "NoReponse")
            case "noResult":
                self.view.showAlert(title: "noResult", message: "noResult")
            case "noData":
                self.view.showAlert(title: "noData", message: "noData")
            default:
                self.view.showAlert(title: "default", message: "default")
            }
        }
    }
    
    func getUser() {
        getUserDetails { [weak self]  (userArray) in
            self?.view.setUser(image: userArray.image ?? "", login: "Login: \(userArray.login ?? "")", name: "Name: \(userArray.name ?? "")", location: "Location: \(userArray.location ?? "")", repos: "Pablic Repository: \(String(describing: userArray.repos ?? 0))", followers: "Followers: \(String(describing: userArray.followers ?? 0))", following: "Following: \(String(describing: userArray.following ?? 0))", nameUser: userArray.login ?? "")
            self!.view.hiddenLoadIndicator()
        }
    }
}
