//
//  PresenterUsers.swift
//  InternThree
//
//  Created by Murtazaliev Shamil on 05/04/2019.
//  Copyright © 2019 Murtazaliev Shamil. All rights reserved.
//

import Foundation

protocol PresenterUsersProtocol{
    func getListUsers(callback: @escaping ([User]) -> Void)
}

class PresenterUsers: PresenterUsersProtocol {
    private var modelUsers: ListUsersProtocol!
    private weak var view: UsersVCProtocol!
    
    init(view: UsersVCProtocol) {
        modelUsers = ListUsers()
        self.view = view
    }
    
    func getListUsers(callback: @escaping ([User]) -> Void) {
        self.view.showLoadIndicator()
        var usersArray = [User]()
        let url = URL(string: "https://api.github.com/users")
        modelUsers.getListUsers(api: url!, success: { (result: [User]) in
            usersArray = result
            callback(usersArray)
            self.view.hiddenLoadIndicator()
        }) { (failure) in
            switch failure {
            case "noResponse":
                self.view.showAlert(title: "NoResponse", message: "NoReponse")
            case "noResult":
                self.view.showAlert(title: "noResult", message: "noResult")
            case "noData":
                self.view.showAlert(title: "noData", message: "noData")
            default:
                self.view.showAlert(title: "default", message: "default")
            }
        }
    }
} 
