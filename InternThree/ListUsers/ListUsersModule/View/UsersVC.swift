//
//  UsersVC.swift
//  InternThree
//
//  Created by Murtazaliev Shamil on 03/04/2019.
//  Copyright © 2019 Murtazaliev Shamil. All rights reserved.
//

import UIKit
import SDWebImage

protocol UsersVCProtocol: class {
    func showAlert(title: String, message: String)
    func showLoadIndicator()
    func hiddenLoadIndicator()
}

final class UsersVC: UIViewController, UsersVCProtocol {
    
    private var presenter: PresenterUsersProtocol!
    var users: [User] = []
    var userDetails: [UserDetails] = []
    var urlUser: String!
    
    @IBOutlet weak var progressIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem?.tintColor = UIColor.black
        presenter = PresenterUsers(view: self)
        presenter.getListUsers(callback: { [weak self] (users)  in
            self!.users = users
            self!.tableView.reloadData()
        })
    }
    
    
    @IBAction func backInMainController (_ sender: Any) {
        let alert = UIAlertController(title: "Выход", message: "Вы уверенны что хотите выйти?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (result) in
            if result.title == "Yes" {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainController") as! MainController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }))
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        present(alert, animated: true)
        //        let vc = storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        //        navigationController?.pushViewController(vc, animated: true)
    }
}

extension UsersVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? UserTVCell else {fatalError()}
        let user = users[indexPath.row]
        cell.imageUser.sd_setImage(with: URL(string: user.image))
        cell.labelName.text = user.name
        cell.labelEmail.text = user.email
        cell.labelLorem.text = user.lorem
        return cell
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "segueInDetailsUser", sender: users[indexPath.row])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueInDetailsUser" {
            if let userDetailsVC = segue.destination as? DetailsUserVC {
                let currentUser = sender as! User
                userDetailsVC.profileId = currentUser.name
            }
        }
    }
    
    func showLoadIndicator() {
        self.tableView.isHidden = true
        progressIndicator.startAnimating()
        progressIndicator.isHidden = false
    }
    
    func hiddenLoadIndicator() {
        progressIndicator.stopAnimating()
        progressIndicator.isHidden = true
        self.tableView.isHidden = false
    }
}
