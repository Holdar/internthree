//
//  RegistrationVC.swift
//  InternThree
//
//  Created by Murtazaliev Shamil on 03/04/2019.
//  Copyright © 2019 Murtazaliev Shamil. All rights reserved.
//

import UIKit

protocol RegistrationVCProtocol: class {
    func showAlert(title: String, message: String)
}

final class RegistrationVC: UIViewController, RegistrationVCProtocol {

    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    private var presenterRegistration: PresenterRegistrationProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem?.tintColor = UIColor.black
        presenterRegistration = PresenterRegistration(view: self)
        passwordTextField.isSecureTextEntry = true
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true)
    }

    @IBAction func backInMainController(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainController") as! MainController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func registration(_ sender: Any) {
        presenterRegistration.saveLoginAndPassword(login: loginTextField.text!, password: passwordTextField.text!, firstName: firstNameTextField.text!, lastName: lastNameTextField.text!)
        performSegue(withIdentifier: "segueTable", sender: nil)
    }
}
