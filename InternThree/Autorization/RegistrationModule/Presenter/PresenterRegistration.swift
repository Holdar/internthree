//
//  PresenterRegistration.swift
//  InternThree
//
//  Created by Murtazaliev Shamil on 03/04/2019.
//  Copyright © 2019 Murtazaliev Shamil. All rights reserved.
//

import Foundation

protocol PresenterRegistrationProtocol {
    func saveLoginAndPassword(login: String, password: String, firstName: String, lastName: String)
}

class PresenterRegistration: PresenterRegistrationProtocol {
    private var modelRegistrationUser: RegistrationUserProtocol
    private weak var view: RegistrationVCProtocol!
    
    init(view: RegistrationVCProtocol) {
        self.view = view
        modelRegistrationUser = RegistrationUser()
    }
    
    func saveLoginAndPassword(login: String, password: String, firstName: String, lastName: String) {
        if login.count < 5 || password.count < 5 || firstName.count < 5 || lastName.count < 5 {
            view.showAlert(title: "Ошибка при вводе данных", message: "Введите больше символов")
        }else {
            if login == "" || password == "" || firstName == "" || lastName == "" {
                view.showAlert(title: "Ошибка при вводе данных", message: "Вы ввели не все данные, введите все данные и попробуйте снова")
            }else{
                modelRegistrationUser.setLoginAndPassword(login: login, password: password)
            }
        }
    }
}
