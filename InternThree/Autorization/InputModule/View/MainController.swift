//
//  ViewController.swift
//  InternThree
//
//  Created by Murtazaliev Shamil on 03/04/2019.
//  Copyright © 2019 Murtazaliev Shamil. All rights reserved.
//

import UIKit

protocol ViewControllerProtocol: class {
    func showAlert(title: String, message: String)
}

final class MainController: UIViewController, ViewControllerProtocol {

    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    private var presenter: PresenterMainControllerProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = PresenterMainController(view: self)
    }

    @IBAction func registarion(_ sender: Any) {
        performSegue(withIdentifier: "segueInRegistrationVC", sender: nil)
    }
    
    @IBAction func login(_ sender: Any) {
        presenter.compareLoginAndPassword(login: loginTextField.text!, password: passwordTextField.text!)
        performSegue(withIdentifier: "segueInTable", sender: nil)
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true)
    }
}

