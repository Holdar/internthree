//
//  PresenterMainController.swift
//  InternThree
//
//  Created by Murtazaliev Shamil on 03/04/2019.
//  Copyright © 2019 Murtazaliev Shamil. All rights reserved.
//

import Foundation

protocol PresenterMainControllerProtocol {
    func compareLoginAndPassword(login: String, password: String)
}

class PresenterMainController: PresenterMainControllerProtocol {
    
    private var modelRegistrationUser: RegistrationUserProtocol!
    private weak var view: ViewControllerProtocol!
    
    init(view: ViewControllerProtocol) {
        self.view = view
        modelRegistrationUser = RegistrationUser()
    }
    
    func compareLoginAndPassword(login: String, password: String) {
        var dict = modelRegistrationUser.getLoginAndPassword()
        if login.count < 5 || password.count < 5 {
            view.showAlert(title: "Ошибка при вводе данных", message: "Введите больше символов")
        }else{
            if login != dict["login"] || password != dict["password"] {
                self.view.showAlert(title: "Ошибка при вводе данных", message: "Неверный логин или пароль, введите верные данные и попробуйте снова!")
            }
        }
    }
    
}
