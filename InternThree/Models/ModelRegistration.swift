//
//  ModelRegistration.swift
//  InternThree
//
//  Created by Murtazaliev Shamil on 03/04/2019.
//  Copyright © 2019 Murtazaliev Shamil. All rights reserved.
//

import Foundation

let userDefaults = UserDefaults.standard

protocol RegistrationUserProtocol {
    func setLoginAndPassword(login: String, password: String)
    func getLoginAndPassword() -> [String:String]
}

class RegistrationUser: RegistrationUserProtocol {
    let login = "login"
    let password = "password"
    
    func setLoginAndPassword(login: String, password: String) {
        userDefaults.set(login, forKey: self.login)
        userDefaults.set(password, forKey: self.password)
    }
    
    func getLoginAndPassword() -> [String:String] {
        let dictLoginAndPassword = [self.login: userDefaults.string(forKey: self.login), self.password: userDefaults.string(forKey: self.password)]
        return dictLoginAndPassword as! [String : String]
    }
}

