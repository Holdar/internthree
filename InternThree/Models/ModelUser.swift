//
//  ModelUser.swift
//  InternThree
//
//  Created by Murtazaliev Shamil on 10/04/2019.
//  Copyright © 2019 Murtazaliev Shamil. All rights reserved.
//

import Foundation
import Alamofire

struct UserDetails: Decodable {
    let image: String?
    let login: String?
    let name: String?
    let location: String?
    let repos: Int?
    let followers: Int?
    let following: Int?
    
    enum CodingKeys: String, CodingKey {
        case image = "avatar_url"
        case login
        case name
        case location
        case repos = "public_repos"
        case followers
        case following
    }
//
//    required init(from decoder: Decoder) throws {
//        let value = try decoder.container(keyedBy: CodingKeys.self)
//        image = try value.decode(String.self, forKey: .image)
//        login = try value.decode(String.self, forKey: .login)
//        name = try value.decode(String.self, forKey: .name)
//        location = try value.decode(String.self, forKey: .location)
//        repos = try value.decode(Int.self, forKey: .repos)
//        followers = try value.decode(Int.self, forKey: .followers)
//        following = try value.decode(Int.self, forKey: .following)
//    }
}
    protocol UserDetailsProtocol {
        func getUserDetails<T: Decodable>(url: URL, success: @escaping (T) -> Void, failure: @escaping (String) -> Void)
    }

class ControllerUserDetails: UserDetailsProtocol {
    
    func getUserDetails<T: Decodable>(url: URL, success: @escaping (T) -> Void, failure: @escaping (String) -> Void) {
        request(url, method: .get).responseData {(respons) in
            if respons.result.isSuccess {
                if let data = respons.data {
                    do {
                        let result = try JSONDecoder().decode(T.self, from: data)
                        success(result)
                    } catch {
                        failure("noResult")
                    }
                } else  {
                    failure("noData")
                }
            }else {
                failure("noRespons")
            }
        }
    }
}
