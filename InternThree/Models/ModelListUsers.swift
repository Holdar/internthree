//
//  ModelListUsers.swift
//  InternThree
//
//  Created by Murtazaliev Shamil on 03/04/2019.
//  Copyright © 2019 Murtazaliev Shamil. All rights reserved.
//

import Alamofire

struct User: Decodable {
    let image: String
    let name: String
    let email: String
    let lorem: String
    let url: String
    
    enum CodingKeys: String, CodingKey {
        case image = "avatar_url"
        case name = "login"
        case email = "html_url"
        case lorem = "repos_url"
        case url
    }
    
//    required init (from decoder: Decoder) throws {
//        let value = try decoder.container(keyedBy: CodingKeys.self)
//        image = try value.decode(String.self, forKey: .image)
//        name = try value.decode(String.self, forKey: .name)
//        email = try value.decode(String.self, forKey: .email)
//        lorem = try value.decode(String.self, forKey: .lorem)
//        url = try value.decode(String.self, forKey: .url)
//    }
}

protocol ListUsersProtocol {
    func getListUsers<T: Decodable>(api: URL, success: @escaping (T) -> Void, failure: @escaping (String) -> Void)
}

class ListUsers: ListUsersProtocol {
    func getListUsers<T: Decodable>(api: URL, success: @escaping (T) -> Void, failure: @escaping (String) -> Void) {
        request(api, method: .get).responseData {(response) in
            if response.result.isSuccess {
                if let data = response.data {
                    do{
                    let result = try JSONDecoder().decode(T.self, from: data)
                    success(result)
                    }catch {
                        failure("noResult")
                    }
                }else {
                    failure("noData")
                }
            }else {
                failure("noRespons")
            }
        }
    }
}
